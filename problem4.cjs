function getCarDetails(inventory) {
  if (inventory && Array.isArray(inventory)) {
    let years = [];

    for (let index = 0; index < inventory.length; index++) {
      years[index] = inventory[index].car_year;
    }
    return years;
  } else {
    return [];
  }
}

module.exports = getCarDetails;
