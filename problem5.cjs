function getCarDetails(inventory, result) {
  if (
    inventory &&
    result &&
    Array.isArray(inventory) &&
    Array.isArray(result)
  ) {
    let olderCarsYear = [];
    let index1 = 0;
    for (let index = 0; index < result.length; index++) {
      if (result[index] < 2000) {
        olderCarsYear[index1++] = result[index];
      }
    }
    return olderCarsYear;
  } else {
    return [];
  }
}

module.exports = getCarDetails;
