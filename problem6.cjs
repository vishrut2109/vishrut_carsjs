function getCarDetails(inventory) {
  if (inventory && Array.isArray(inventory)) {
    let BMWAndAudi = [];
    let index1 = 0;
    for (let index = 0; index < inventory.length; index++) {
      if (
        inventory[index].car_make == "BMW" ||
        inventory[index].car_make == "Audi"
      ) {
        BMWAndAudi[index1++] = inventory[index];
      }
    }
    return BMWAndAudi;
  } else {
    return [];
  }
}

module.exports = getCarDetails;
