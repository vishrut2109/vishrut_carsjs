const CarDetails = require("../problem2.cjs");

const inventory = require("../cars.cjs");

const result = CarDetails(inventory);

console.log(
  `Last Car is a  ${inventory[result].car_make} ${inventory[result].car_model} `
);
