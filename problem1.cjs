function getCarDetails(inventory, id) {
  if (id === 0) {
    if (inventory && typeof id === "number" && Array.isArray(inventory)) {
      for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id === id) {
          return inventory[index];
        }
      }
    } else {
      return [];
    }
  } else {
    if (inventory && id && typeof id === "number" && Array.isArray(inventory)) {
      for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id === id) {
          return inventory[index];
        }
      }
    } else {
      return [];
    }
  }
}

module.exports = getCarDetails;
