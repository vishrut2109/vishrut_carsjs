function getCarDetails(inventory) {
  if (inventory && Array.isArray(inventory)) {
    const last = inventory.length - 1;
    return last;
  } else {
    return null;
  }
}

module.exports = getCarDetails;
