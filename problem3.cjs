function getCarDetails(inventory) {
  if (inventory && Array.isArray(inventory)) {
    let model = [];
    for (let index = 0; index < inventory.length; index++) {
      model[index] = inventory[index].car_model;
    }

    model.sort();
    return model;
  } else {
    return [];
  }
}

module.exports = getCarDetails;
